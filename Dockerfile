FROM golang AS builder
RUN apt-get update && apt-get install -y git upx binutils

WORKDIR /go/src/github.com/nmarcetic/docker-nginx-reload
COPY . ./
RUN go get -d -v
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -a -o /go/bin/docker-nginx-reload
RUN strip /go/bin/docker-nginx-reload
RUN upx -v --ultra-brute /go/bin/docker-nginx-reload

############################

FROM ubuntu
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /go/bin/docker-nginx-reload /docker-nginx-reload
ENTRYPOINT ["/docker-nginx-reload"]